const nodemailer = require('nodemailer')

class SendEmail {
  constructor(host, user, pass) {
    this.transport = nodemailer.createTransport({
      host,
      port: 465,
      secure: true,
      auth: {user, pass},
    })
  }

  send({from, to, data, callback}) {
    try {
      return this.transport.sendMail(
          {
            envelope: {from, to},
            from,
            to,
            subject: data.subject ? data.subject : 'No mail subject',
            html: data.message ? data.message : 'No mail message',
            attachments: data.attachments ? data.attachments : [],
          }, callback,
      )
    } catch (e) {
      throw new Error(e.message)
    }
  }
}

module.exports = {SendEmail}
