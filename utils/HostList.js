const axios = require('../axios')

class HostList {
  constructor() {
    this.hosts = {
      main: [
        {host: 'https://node1.foil.network:9077'},
        {host: 'https://node2.foil.network:9077'},
        {host: 'https://node3.foil.network:9077'},
        {host: 'https://node4.foil.network:9077'},
        {host: 'https://node5.foil.network:9077'},
        {host: 'https://node6.foil.network:9077'},
        {host: 'https://node7.foil.network:9077'},
        {host: 'https://node8.foil.network:9077'},
      ],
      test: [
        {host: 'https://node1.foil.network:9087'},
        {host: 'https://node2.foil.network:9087'},
        {host: 'https://node3.foil.network:9087'},
        {host: 'https://node4.foil.network:9087'},
        {host: 'https://node5.foil.network:9087'},
        {host: 'https://node6.foil.network:9087'},
        {host: 'https://node7.foil.network:9087'},
        {host: 'https://node8.foil.network:9087'},
      ],
      update: null,
    }
  }

  get getHosts() {
    return this.hosts
  }

  get getMain() {
    return this.hosts.main
  }

  get getTest() {
    return this.hosts.test
  }

  start() {
    this.update()
        .then(() => {
          setInterval(async () => {
            await this.update()
          }, 5 * 60 * 1000)
        })
  }

  update() {
    this.hosts.update = new Date()
    return Promise.all([
      this.baseUpdate('main'),
      this.baseUpdate('test'),
    ])
  }

  baseUpdate(type) {
    const hosts = []
    return Promise.all(this.hosts[type].map(h => {
      return axios.get(h.host + '/api/lastblock')
          .then(response => {
            hosts.push({
              'host': h.host,
              'height': response.data.height || null,
              'status': response.status || null,
            })
          })
          .catch(() => {
            hosts.push({
              'host': h.host,
              'height': null,
              'status': null,
            })
          })
    })).then(() => this.hosts[type] = hosts)
  }
}

module.exports = {HostList}
