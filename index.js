require('dotenv').config()
const express = require('express')
const formidableMiddleware = require('express-formidable')
const {HostList} = require('./utils/HostList')
const {SendEmail} = require('./utils/SendEmail')
const server = express()
const hosts = new HostList()
hosts.start()

const email = new SendEmail(
    process.env.MAIL_SMTP,
    process.env.MAIL_USER,
    process.env.MAIL_PASS,
)

server.use(formidableMiddleware())

server.use((request, response, next) => {
  response.setHeader('Access-Control-Allow-Origin', '*')
  response.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS, POST')
  response.setHeader(
      'Access-Control-Allow-Headers',
      'X-Requested-With,content-type',
  )
  response.setHeader('Access-Control-Allow-Credentials', 'true')
  next()
})

server.get('/get-hosts/', (request, response) => {
  response.json(hosts.getHosts)
})

server.post('/send-email', async (request, response) => {
  const attachments = []
  for (const [_, file] of Object.entries(request.files)) {
    attachments.push({
      filename: file.name,
      path: file.path,
    })
  }

  const {to, subject, message} = request.fields

  if (subject && message) {
    await email.send({
      from: process.env.MAIL_USER,
      to,
      data: {subject, message, attachments},
      callback: err => err
          ? response.json({error: 'not working send email', message: err})
          : response.json('ok'),
    })
  }
})

server.listen(8091)
